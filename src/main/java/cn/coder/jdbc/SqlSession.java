package cn.coder.jdbc;

import java.util.List;

import cn.coder.jdbc.support.JSql;
import cn.coder.jdbc.support.MulitResult;
import cn.coder.jdbc.support.PageResult;
import cn.coder.jdbc.support.ResultMapper;

/**
 * SQL查询、执行工具类
 * 
 * @author YYDF
 *
 */
public interface SqlSession {

	boolean selectMulit(final MulitResult mr);
	
	SqlTranction beginTranction(SqlTranction... tranctions) throws Exception;

	<T> List<T> selectList(final Class<T> target, final JSql sql);

	<T> List<T> selectList(final Class<T> target, final String sql, Object... array);

	<T> List<T> selectPage(final Class<T> target, final PageResult result, final JSql sql);

	<T> List<T> selectPage(final Class<T> target, final PageResult result, final String fetchSql,
			final String countSql, Object... array);

	<T> T selectOne(final Class<T> target, final JSql sql);

	<T> T selectOne(final Class<T> target, final String sql, Object... array);

	boolean exist(final Object data);

	boolean insert(final Object data);

	boolean update(final Object data);

	boolean delete(final Object data);

	Object[] callProcedure(String name, Object... args);

	int execute(final String sql, final Object... array);

	<T> T execute(final ResultMapper<T> mapper);

}
